#include <dht11.h>   // Libreria DHT11
dht11 TEMPE;         //Creamos el objeto TEMPE
#define DHT_out 7    //Asignamos el pin 7 como salida del sensor  
int ventilador=8;
int buzzer=9;
int lectura;
int temperatura;
void setup()
{
  Serial.begin(9600);
  pinMode(8,OUTPUT);
  pinMode(9, OUTPUT);
}

void loop()
{
  lectura = TEMPE.read(DHT_out);   // Leemos los datos del sensor
  int flama = analogRead(A0) ;         // Leemos los datos del S. flama
  temperatura = TEMPE.temperature;
  Serial.print(temperatura);
  delay(500);
  if(flama < 1000){
    digitalWrite(ventilador,HIGH);
    digitalWrite(buzzer,HIGH);
    Serial.print('A');
  }
  else{
    digitalWrite(ventilador,LOW);
    digitalWrite(buzzer,LOW);
    Serial.print('B');
  }
  delay(500);
}

