#include <dht11.h> // Libreria DHT11
dht11 TEMPE; //Creamos el objeto TEMPE
#define DHT_out 7  //Asignamos el pin 7 como salida del sensor  
int ventilador=8;
int buzzer=9;
int lectura;
int temperatura;
void setup()
{
  Serial.begin(9600);
  pinMode(8,OUTPUT);
  pinMode(9, OUTPUT);}

void loop()
{
  lectura = TEMPE.read(DHT_out);   //Leemos los datos del sensor
  int flama = analogRead(A0) ; 
  //Mostramos la temperatura
  temperatura = TEMPE.temperature;
  if(flama<1000){
    digitalWrite(ventilador,HIGH);
    digitalWrite(buzzer,HIGH);
    Serial.println("Bazer prendido");
  }
  else{
    digitalWrite(ventilador,LOW);
    digitalWrite(buzzer,LOW);
    Serial.println("Bazer apagado");
  }
  Serial.print(" Temperatura: ");
  Serial.print(temperatura);
  Serial.println(" C");
  delay(500); // pequeño retardo para una buena lectura
}

