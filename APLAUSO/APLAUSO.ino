#include <TimerOne.h>

int Led = 13 ;// define LED Interface
int buttonpin = 3; // define D0 Sensor Interface
int val = 0;// define numeric variables val
int aplausos=0; 
void setup ()
{
  Serial.begin(9600);
  pinMode (Led, OUTPUT) ;// define LED as output interface
  pinMode (buttonpin, INPUT) ;// output interface D0 is defined sensor
  Timer1.initialize(1300000);         // Dispara cada 1500000
  Timer1.attachInterrupt(ISR_Blink); // Activa la interrupcion y la asocia a ISR_Blink  
}
 
void loop ()
{
  val = digitalRead(buttonpin);// digital interface will be assigned a value of pin 3 to read val
  if (val == HIGH) // When the sound detection module detects a signal, LED flashes
  {
    aplausos++;
  delay(60);
  }
//      if (aplausos == 1){
//        digitalWrite (Led, HIGH);     
//      }  

}

void ISR_Blink()
   {   
     //ledState = !ledState ;
      // blinkCount++    ;     // Contador veces se enciende el LED
      Serial.println(aplausos);
      
      if (aplausos == 1){
        digitalWrite (Led, HIGH);     
      }
      if (aplausos > 1){
        digitalWrite (Led, LOW);    
      }      
       aplausos=0;
   }
