#include <TimerOne.h>  //Librería para INTERRUPCION TIMER

int buttonpin = 3;  // Pin de la ENTRADA DIGITAL
int val = 0;        // Estado variable de la lectura
int aplausos=0;     // Contador de aplausos
int blink_=0;       // Variable para ayudar en el PARPADEO
int estado=HIGH;    // Variable para ayudar en el PARPADEO

void setup ()
{//Configurando
  Serial.begin(9600);      // Inicia el Serial, si se desea visualizar en el MONITOR SERIAL
  pinMode (13, OUTPUT) ;   // El pin 13 en modo SALIDA
  pinMode (12, OUTPUT) ;   // El pin 12 en modo SALIDA
  pinMode (11, OUTPUT) ;   // El pin 11 en modo SALIDA 
  pinMode (10, OUTPUT) ;   // El pin 10 en modo SALIDA
  pinMode (9, OUTPUT) ;    // El pin 9del LED en modo SALIDA       
  pinMode (buttonpin, INPUT) ;      // output interface D0 is defined sensor
  Timer1.initialize(600000);        // Dispara cada 600 ms la interrupción TIMER
  Timer1.attachInterrupt(APLAUSOS); // Activa la interrupción y la asocia a APLAUSOS  
}
 
void loop ()
{//Instrucciones
  val = digitalRead(buttonpin);  // Carga el estado leído
  if (val == HIGH)               // ¿Senso?
  {
  aplausos++;                    // Incrementa contador "aplauso"
  Serial.println(aplausos);      // Muestra el contador
  }
}
void APLAUSOS()
   {  //Invierte el estado del ultimo LED para el PARPADEO de este
      estado=!estado;            
      if(blink_ == 13){digitalWrite (blink_, estado);}  
      if(blink_ == 12){digitalWrite (blink_, estado);}
      if(blink_ == 11){digitalWrite (blink_, estado);}
      if(blink_ == 10){digitalWrite (blink_, estado);}
      if(blink_ == 9){digitalWrite (blink_, estado);}
      //Si hay aplausos:
      if (aplausos>0){
        if (aplausos < 10){         //Si es muy debil el sonido
        digitalWrite (13, HIGH);blink_=13;
        digitalWrite (12, LOW);
        digitalWrite (11, LOW);
        digitalWrite (10, LOW);
        digitalWrite (9, LOW);}
        else if (aplausos <20 ){    //Si es debil el sonido
          digitalWrite (13, HIGH);
          digitalWrite (12, HIGH);blink_=12;
          digitalWrite (11, LOW);
          digitalWrite (10, LOW);
          digitalWrite (9, LOW);
        }
          else if (aplausos <30 ){  //Si es fuerte el sonido
            digitalWrite (13, HIGH);  
            digitalWrite (12, HIGH);
            digitalWrite (11, HIGH);blink_=11;
            digitalWrite (10, LOW);
            digitalWrite (9, LOW);        
          }
            else if (aplausos <40 ){//Si es muy fuerte el sonido
            digitalWrite (13, HIGH);
            digitalWrite (12, HIGH);
            digitalWrite (11, HIGH);
            digitalWrite (10, HIGH);blink_=10;
            digitalWrite (9, LOW);          
            }
              else{                 //Si es demasiado fuerte el sonido
              digitalWrite (13, HIGH);
              digitalWrite (12, HIGH);
              digitalWrite (11, HIGH);
              digitalWrite (10, HIGH);
              digitalWrite (9, HIGH);blink_=9;}              
      }
    aplausos=0;                     //Reinicia el contador
   }

